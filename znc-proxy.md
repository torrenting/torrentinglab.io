# Setup ZNC IRC Proxy

## Install ZNC

1.  Add the ZNC PPA to the system to ensure you get the latest version
    ```
    sudo add-apt-repository ppa:teward/znc
    ```

2.  Install ZNC
    ```
    sudo apt install znc znc-perl znc-python znc-tcl
    ```