# Summary

----

*  [Server Setup](server/setup.md)
    *  [Install Ubuntu Server](server/setup.md#install-ubuntu-server)
    *  [Public Key Authentication](server/public-key-authentication.md)
        *  [Generate SSH Login Key on Client Computer](server/public-key-authentication.md#generate-ssh-login-key-on-client-computer)
        *  [Secure SSH Server](server/public-key-authentication.md#secure-ssh-server)
    *  [Update System](server/setup.md#update-system)
    *  [Configure Firewal](server/setup.md#configure-firewall)
    *  [Create a SFTP User](server/setup.md#create-a-sftp-user)
    *  [Setup File Storage](server/setup.md#setup-file-storage)
        *  [Setup Shared Storage](server/file-system.md)
        *  [Test & Format Drives](server/test-format-drives.md)
    *  [Setup OpenVPN Client](server/setup.md#setup-openvpn-client)
        *  [Private Internet Access (PIA)](server/openvpn-client-PIA.md)
        *  [AirVPN](server/openvpn-client-AirVPN.md)

----

*  [Daemon Setup](daemon/readme.md)
    *  [Deluge](daemon/deluge.md)
    *  [rTorrent](daemon/rtorrent.md)

----

*  [Share Your Files](sharing/index.md)
    *  [Setup NFS](sharing/nfs.md)