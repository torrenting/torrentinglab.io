# Test & Format Drives

## Test Drives

1. Install smartmontools
    ```
    sudo apt install smartmontools
    ```
2. List all connected drives
    ```
    sudo lsblk
    ```
3. Run a short test to ensure the drive is functioning correctly
    ```
    sudo smartctl -t short /dev/device
    ```
4. After about 2 minutes the test should be done. View the results and SMART statistics
    ```
    sudo smartctl -a /dev/device
    ```
5. Check the following SMART statistics for any non zero results in the RAW_VALUE column. A non zero result can indicate a failing or damaged drive. 
    *  Reallocated_Sector_Ct (5)
    *  Reported_Uncorrect (187)
    *  Command_Timeout (188)
    *  Current_Pending_Sector (197)
    *  Offline_Uncorrectable (198)

## Stress Test & Check for Bad Sectors
If you have brand new drives and want to make sure they won't immediately fail you can stress test them.  
Drives that are damaged in shipping or that will fail within a the return period might fail during the stress test saving you from premature failure later.  
Note that this will write and read the entire drive 4 times which can take several days on slow and large drives (8TB)  
Ensure that your drive has sufficient airflow to cool it or you can cause damage to the drive. Most cheap external drives do not have any airflow and should be removed from the case first.  
Do not use on drives with data!

1. Start a screen session so the testing process continues if you get disconnected or logout.
   ```
   screen
   ```
2. List all drives
    ```
    sudo lsblk
    ```
3. Run badblocks to check for disk surface issues. This process will wipe all data on the disk so triple check you have the right disk!
    ```
    sudo badblocks -w -v -s -b 4096 -o badblocks.log /dev/<device>
    ```
4. Detatch the currently running screen so it runs in the background. Press Ctrl+A then press D
    * You can check on the progress at any time by entering the following command
        ```
        screen -r
        ```
    * If you had multiple screens detached then it will list all active ones. Copy the name of the one you want (####.pts-0.hostname) then enter
        ```
        screen -r ####.pts-0.hostname
        ```
5. When you’re finished enter “exit” to close the currently open screen or press Ctrl+A then K.


## Format Drives

1. Get a list of all connected drives
    ```
    sudo lsblk
    ```
2. Choose a filesystem:

    ### Ext4:
    *  Linux filesystem
    *  Stable, reliable, and performant
    *  Lacks advanced features

        ```
        sudo mkfs.ext4 -b 4096 -m 1 -L volume_label /dev/<device>
        ```
        *  The -b option sets the block size which is commonly 4096 bytes on modern hard drives.
        *  The -m 1 option sets 1% reserved space for the superuser. Only really needed to prevent fragmentation on non system drives. Defaults to 5%.
        *  The -L option sets the label for the volume.

    ### NTFS:
    *  Windows filesystem
    *  Compatible with Windows and Linux. Has been reverse engineered to work in linux
    *  Lacks advanced features.

    ### XFS:
    *  Highly performant multiplatform filesystem
    *  Great alternative to Ext4 when fast parallel disk acess is required
    *  Resists fragmentation
    *  No snapshot support

    ### Btrfs:
    *  Next generation Linux filesystem
    *  Copy-on-write (COW)
    *  Supports snapshots
    *  All blocks are automatically checksummed allowing for detection of corruption and data loss
    *  On-demand file integrity checking
    *  Resistant to fragmentation
    *  Optional RAID 0, RAID 1, RAID 10 (RAID 5 is not production ready yet)

        ```
        sudo mkfs.btrfs -L "myLabel" /dev/<device>
        ```
        *  The -L option sets the label for the volume.

    ### ZFS:
    *  Advanced enterprise grade filesystem
    *  Snapshots
    *  Automatic file integrity monitoring and repair
    *  Drive pooling
    *  Requires lots of RAM and high performance hardware or you will experience poor performance. Designed to protect data at all costs.
    *  Advanced users only

## Configure Automatic Mounting

1. List all volumes with their UUIDs
    ```
    sudo lsblk -o SIZE,MODEL,NAME,UUID
    ```
2. Modify fstab
    ```
    sudo nano /etc/fstab
    ```
3. Add lines for each device.
    ```
    /dev/disk/by-uuid/device_uuid mount_point file_system rw,auto,noexec,nouser,async,nodev,nosuid 0 2
    ```
    *  Replace device_uuid with the UUID of your drive
    *  Replace mount_point with the directory name where it should be mounted
    *  Replace file_system with the one for your chosen fs (ext4, btrfs,etc)

