# Setup Public Key Authentication

The SSH service is frequently attacked on servers.  
Using a password to login to SSH is basic security.  
Using a password encrypted key is considerably safer since no password is ever transmitted to or stored on the server.  
Your private key on your computer is used to sign a challenge sent by the server which is then transmitted to the server for verification with the stored public key.

## Generate SSH Login Key on Client Computer

Choose a section below depending on which Operating System you're using to connect to the server.

### Windows Client

1.  If this is your first time generating SSH keys make sure you have PuTTY and PuTTYGen installed.
2.  Open PuTTYGen. Ensure the options at the bottom are set to RSA and bits in key are at least 2048.
3.  Click “Generate” to create a rsa key:
    1.  “Key comment” should be *username*@*server_ip*.
    2.  Enter a “Key passphrase” to encrypt the private key with. You will need to enter this password every time you connect to your server.
    3.  Save your private key file somewhere you won't lose it. It is safe to upload to a sync provider such as Google Drive because it is encrypted.
4.  Connect to the server via SSH:
    1.  Start PuTTY and login using your *username* and *server_ip*.
    2.  PuTTY will ask you to verify the server's fingerprint. Enter the following on the server console to see the server's fingerprint:
        ```
        ssh-keygen -l -E md5 -f /etc/ssh/ssh_host_ed25519_key.pub
        ```
    3.  Check if the fingerprint matches. If it matches then click Yes to proceed. If it does not match then someone is impersonating your server or you are connecting to the wrong server.
    4.  Enter your password when prompted.
    6.  Copy the public key from PuTTYGen (box at the top) for the [next step](server/public-key-authentication.md#add-the-ssh-key-to-your-account).

### Linux Client

1.  If this is the first time generating keys on the client computer create the .ssh directory:
    ```
    mkdir ~/.ssh
    chmod u=rwX,g=,o= .ssh
    ```

2.  Generate a rsa key:
    ```
    ssh-keygen -t rsa -f ~/.ssh/username@server_ip
    ```
    You will be asked to enter a password to encrypt the key. Store this somewhere safe such as in a password manager.

3.  Display the public key. Save this for the next step:
    ```
    cat ~/.ssh/username@server_ip.pub
    ```

4.  Connect to the server via SSH:
    ```
    ssh -o PubkeyAuthentication=no username@server_ip
    ```
    1.  SSH will ask you to verify the server's fingerprint. Enter the following on the server console to see the server's fingerprint:
        ```
        ssh-keygen -lf /etc/ssh/ssh_host_ecdsa_key.pub
        ```
    3.  Check if the fingerprint matches. If it matches then type yes and hit enter. If it does not match then someone is impersonating your server or you are connecting to the wrong server.
    4.  Enter the password for *username*.
    5.  Add your public key in the [next step](server/public-key-authentication.md#add-the-ssh-key-to-your-account).

## Add the SSH Key to your Account
1.  Create the .ssh directory:
    ```
    mkdir .ssh
    chmod u=rwX,g=,o= .ssh
    ```
2.  Paste the public key into the authorized_keys file on the server:
    ```
    echo "your_public_key" >> .ssh/authorized_keys
    ```
3.  Change the permissions so only your user can read the file:
    ```
    chmod u=rw,g=,o= .ssh/authorized_keys
    ```
4.  Logout and then login using your SSH key in the [next step](server/public-key-authentication.md#login-using-your-key).

## Login Using Your Key
    
### Windows Client

1.  Open PuTTY. In the side menu expand Connection then SSH. Click on Auth. Click Browse next to the box labeled “Private key file for authentication”.
2.  Find your key and open it
3.  Connection then Data. Enter the username for Auto-login username
4.  Go back to the Session section in the side menu
5.  Enter the IP address of the server
6.  In the box below “Saved Sessions” enter a name for this session
7.  Click Save to store your settings
8.  Now you can double click your saved session in the list to open it

### Linux Client

```
ssh -i ~/.ssh/private_key_file -o IdentitiesOnly=yes username@server_ip
```

## Secure SSH Server

Disable clear-text password logins to greatly improve the security of your server.  
Logging in as root over SSH is considered a bad security practice. Disabling root login completely is recommended.

1.  Edit the SSH server configuration file:
    ```
    sudo nano /etc/ssh/sshd_config
    ```

2.  Disable logging in as root:  
    Find the line `#PermitRootLogin prohibit-password`  
    Change to `PermitRootLogin no`

3.  Disable clear-text password logins:  
    Find the line `#PasswordAuthentication yes`  
    Change to `PasswordAuthentication no`

4.  Save the configuration file (Ctrl+O).
5.  Test your configuration file for errors:
    ```
    sudo sshd -t
    ```
    If you get no output then there are no errors.

5.  Restart the SSH server:
    ```
    sudo systemctl restart ssh
    ```

### [>> Next Step: Update System](/server/setup.html#update-system)