# Setup OpenVPN Client for AirVPN

## Install OpenVPN

1.  Install openvpn:
    ```
    sudo apt install openvpn
    ```

## Generate a Configuration File
1.  Go to the AirVPN configuration generator https://airvpn.org/generator/
2.  Create a config file making sure to check the following
    *  Advanced Mode
    *  Linux
    *  OpenVPN Version >=2.4
    *  IPv4 & IPv6 (connect with IPv4)
    *  Protocols: Check UDP 1194 only
    *  Select a region

3.  Download the configuration file.

## Upload Configuration File to Server

1.  Open the downloaded configuration file in a text editor. Do not use Notepad.
2.  Paste contents into a new configuration file in the openvpn folder. Name your file after the location it connects to:
    ```
    sudo nano /etc/openvpn/airvpn-location.conf
    ```
3.  The config file contains an private key that needs to be kept secret. Change the permissions so only root can read the file:
    ```
    sudo chmod u=rw,go= /etc/openvpn/airvpn-location.conf
    ```

## Setup Firewall for VPN

1.  Enable outbound blocking to prevent leaks if the VPN disconnects:
    ```
    sudo ufw default deny outgoing
    ```

2.  Find the IPs for your configured DNS server\(s\):
    ```
    systemd-resolve --status
    ```

3.  Allow outbound connections to the DNS server\(s\). Run the command for each IP:
    ```
    sudo ufw allow out on interface_name to dns_server_ip port 53 comment "DNS"
    ```

1.  Allow the VPN connection through the firewall:
    ```
    sudo ufw allow out on <lan_interface> to any proto udp port 1194 comment "OpenVPN"
    ```

2.  Allow all outbound connections over the tunnel:
    ```
    sudo ufw allow out on tun0 to any comment "Outbound Tunneled Traffic"
    ```

## Test Your Configuration File

1.  Run OpenVPN with your configuration file:
    ```
    sudo openvpn /etc/openvpn/airvpn-location.conf
    ```
    If you see "Initialization Sequence Completed" then the connection has been successful. Press Ctrl+C to quit.  
    If you get an error check your firewall settings and the port in the openvpn config file.

## Enable OpenVPN On Boot

1.  Check your current IP address by visiting [this site](https://icanhazip.com) in a browser. Make a note. This is your ISP issued IP address.

2.  Enable the systemd service for your config file to automatically start on boot:
    ```
    sudo systemctl enable openvpn@airvpn-location.service
    ```

3.  Start the openvpn service:
    ```
    sudo systemctl start openvpn@airvpn-location.service
    ```

4.  Check the status to make sure you have connected correctly:
    ```
    sudo systemctl status -l openvpn@airvpn-location.service
    ```

5.  Check your IP. It should be different than your ISP issued IP address.
    ```
    curl https://icanhazip.com
    ```

## Setup automatic restart of VPN service

1.  Create a service config file to append to the existing configuration
    ```
    sudo systemctl edit openvpn@
    ```

2.  Paste the following into the text editor then save
    ```
    [Service]
    Restart=always
    RestartSec=30
    ```

3.  Reload the service
    ```
    sudo systemctl daemon-reload
    ```

4.  Check your resulting configuration
    ```
    sudo systemctl cat openvpn@
    ```

5. You should see your changes in the section
    ```
    # /etc/systemd/system/openvpn@.service.d/override.conf
    ```

6.  Edit the configuration file to disable openvpn's builtin reconnection feature which almost never works. Comment out the following line:
    ```
    #resolv-retry infinite
    ```
7.  Test the automatic restart by disconnecting from the internet. Check the service status. It might take a minute or two for it to realize the connection is dead. You should see something like “TLS Error: TLS key negotiation failed to occur within 60 seconds (check your network connectivity)”
    ```
    sudo systemctl status -l openvpn@airvpn-location.service
    ```

8.  Now reconnect to the internet and wait a few minutes for it to attempt to reconnect. Rerun the above command. You should now see: Intialization Completed

## Configure Port Fowarding

1.  Go to the [Forwarded ports](https://airvpn.org/ports/) page.

2.  Enter a port number to forward. Ensure you enter the same port for the local port.

3.  Open forwarded port on the tunnel into your firewall.
    ```
    sudo ufw allow in on tun0 to any port port_number comment "Incoming Torrent Traffic"
    ```

4.  Go into your torrent client settings and set the incoming port to the one you forwarded.

### [>> Next Step: Setup File Storage](server/setup.md#setup-file-storage)