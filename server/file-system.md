# Setup Shared Storage

## Create a Share Group

1. Create a group for the shared directories. This will be used by various daemons and users to write shared files.
    ```
    sudo groupadd shares_group
    ```

2. Add users to the shares_group. Make sure to include your admin user and ftp user. 
    ```
    sudo usermod -a -G shares_group user_name
    ```

3. Check if each user has been added to the group. Your currently logged in ssh session will not gain group permissions until you log out and back in.
    ```
    groups user_name
    ```

## Create Storage Directories

1. Create a location to store your files. The recommended location for served files is in ```/srv/```
    ```
    sudo mkdir -p /srv/share_name
    ```

2. Set permissions so the members of the shares_group can access and create files in the shared storage
    ```
    sudo chown admin_user:shares_group /srv/share_name
    chmod ug=rwX,o=rX /srv/share_name
    ```

3. Force all files and directories created in the shared location to have the same group
    ```
    chmod g+s /srv/share_name
    ```

4. Set a default ACL to ensure files are created with the correct group permissions regardless of user's umask
    ```
    setfacl -dm g::rwx /srv/share_name
    ```

5. If your server has multiple storage drives create a directory for each one. Remember to set the permissions on the mount directories after mounting them.
    ```
    mkdir /srv/share_name/mount_point
    ```