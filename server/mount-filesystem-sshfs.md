# Mount a Remote Filesystem Using SSHFS

## Setup SSHFS

If your files are stored on a different server or virtual machine you'll need to remotely mount the folders where your files are stored.

1.  Install SSHFS:
    ```
    sudo apt install sshfs
    ```
2.  Allow outbound SSH connections to your file server. <lan_interface> is your non-vpn interface such as eth0. You can find the name with `ip link show`:
    ```
    sudo ufw allow out on <lan_interface> to <server_ip> port 22 comment "sshfs"
    ```
3.  Create folder to mount remote files:
    ```
    sudo mkdir /srv/<mount_directory>
    ```
4.  Generate a SSH key to use for the connection.
    1.  Create the root account .ssh directory with the following command:
        ```
        sudo mkdir /root/.ssh
        ```
    2.  Generate a rsa key, skip using a passphrase:
        ```
        sudo ssh-keygen -t rsa -f /root/.ssh/<username>@<server_name>
        ```
    3.  Display the public key so you can copy it to the file server:
        ```
        sudo cat /root/.ssh/<username>@<server_name>.pub
        ```
5.  Add a user account on the file server and add the public ssh key to the account.
    1.  Create a user account on the file server with membership in your shared file group:
        ```
        sudo adduser --ingroup <shares_group_name> <username>
        ```
    2.  Switch to the user account and enter the account password:
        ```
        su - <username>
        ```
    3.  Create the .ssh directory:
        ```
        mkdir .ssh
        ```
    4.  Copy the public key from the torrent server and paste into the to the file server authorized_keys file. The key should be a single line and start with ssh-rsa followed by the key material:
        ```
        echo "<public_key>" >> .ssh/authorized_keys
        ```
    5.  Change the permissions so only the user can read the file:
        ```
        chmod u=rw,g=,o= .ssh/authorized_keys
        ```
    6.  Exit the user account and go back to the admin account:
        ```
        exit
        ```
6.  Enter the following on the file server to see the host's ECDSA key fingerprint:
    ```
    ssh-keygen -lf /etc/ssh/ssh_host_ecdsa_key.pub
    ```
7.  On your torrent server connect via ssh and confirm the fingerprint matches. If the fingerprint on the host matches what the client computer says, type yes. If it doesn't then someone is impersonating your server or you're connecting to the wrong server. Do not continue!
    ```
    sudo ssh -i /root/.ssh/<username>@<server_name> <username>@<server_ip>
    ```
8.  Now the server has been added to the list of known hosts and SSHFS will be able to connect without user input. Close the SSH connection.
    ```
    logout
    ```
9.  Test the connection:
    ```
    sudo sshfs <username>@<server_ip>:/<remote_directory> /srv/<mount_directory> -o IdentityFile=/root/.ssh/<username>@<server_name>
    ```
10.  Close the test connection:
    ```
    sudo fusermount -u /srv/<mount_directory>
    ```
11.  Fix umask permission from sshfs

## Configure Automatic Mounting of SSHFS Volumes

1.  Find the deluged username and get the UID & GID of your deluged user
    1.  Display the contents of the user accounts file
        ```
        cat /etc/passwd
        ```
    2.  Find the user account, probably named debain-deluged
    3.  The format is username:x:user_id:group_id::home_directory:terminal_program
2.  Modify fstab
    ```
    sudo nano /etc/fstab
    ```
3.  Add an entry for each remote directory you want to mount
    ```
    <username>@<server_ip>:/<remote_directory> /srv/<mount_directory> fuse.sshfs _netdev,allow_other,default_permissions,uid=USER_ID_N,gid=USER_GID_N,IdentityFile=/root/.ssh/<username>@<server_name>,sshfs_debug 0 0
    ```

    *  `_netdev`: Ensure the network is available before mounting
    * `allow_other`: Allows all users to access the mount point instead of just the mounting user (root)
    * `default_permissions`: Respect the file permissions set on the mount directory. Without it then anyone has full access to the mount point contents regardless of permissions set on the directory.
    * `uid=USER_ID_N`: Set the mount point owner to the specified user id
    * `gid=USER_GID_N`: Set the mount point group to the specified group id
    * `IdentityFile=`: Location of SSH login key
    * `sshfs_debug`: Show some debug information. You can view the debug messages with “sudo systemctl status -l <mount_name>.mount”

4.  Tell systemd to reload service configuration files
    ```
    sudo systemctl daemon-reload
    ```
5.  Get a list of disk mount services. You can find it by looking at the description column for the mount directory.
    ```
    sudo systemctl -all -t mount
    ```
6.  Run the service file to mount the remote filesystem
    ```
    sudo systemctl start srv-<mount_directory>.mount
    ```
7.  Ensure the remote filesystem has mounted properly
    ```
    sudo systemctl status -l srv-<mount_directory>.mount
    ```
8.  Check the mount directory for proper permissions
    ```
    sudo ls -l /srv/<mount_directory>
    ```

