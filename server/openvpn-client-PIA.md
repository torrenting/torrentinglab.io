# Setup OpenVPN Client for Private Internet Access (PIA)

## Disable IPv6

Disable IPv6 since it is not supported by PIA and can cause leaks.

1.  Edit sysctl.conf:
    ```
    sudo nano /etc/sysctl.conf
    ```

2.  Add the following at the bottom of the configuration file to disable IPv6:
    ```
    # IPv6 disabled
    net.ipv6.conf.all.disable_ipv6 = 1
    net.ipv6.conf.default.disable_ipv6 = 1
    net.ipv6.conf.lo.disable_ipv6 = 1
    ```

## Install OpenVPN

1.  Install openvpn, unzip & curl:
    ```
    sudo apt install openvpn unzip curl
    ```

## Download & Unzip Configuration Files
1.  Download the VPN configuration archive:
    ```
    wget https://www.privateinternetaccess.com/openvpn/openvpn.zip
    ```
2.  Extract archive contents:
    ```
    unzip openvpn.zip -d pia-openvpn
    ```

## Choose a Configuration File

1.  List the config files and choose your desired location:
    ```
    ls pia-openvpn/
    ```
2.  Copy configuration file to the openvpn folder:
    ```
    sudo cp pia-openvpn/<Location>.ovpn /etc/openvpn/<Location>.conf
    ```

## Create an Authentication File

1.  Create a file with for your VPN username and password:
    ```
    sudo touch /etc/openvpn/PIA-Authentication.txt
    ```

2.  Change the file permission to ensure only root can read the authentication file:
    ```
    sudo chmod u=rw,g=,o= /etc/openvpn/PIA-Authentication.txt
    ```

3.  Edit the authentication file:
    ```
    sudo nano /etc/openvpn/PIA-Authentication.txt
    ```

4.  Enter your username on the first line and your password on the second line:
    ```
    username
    password
    ```

## Edit OpenVPN Configuration Files

1.  Edit PIA-Location.conf file:
    ```
    sudo nano /etc/openvpn/PIA-<Location>.conf
    ```

2.  Find the line `auth-user-pass`.

3.  Change to `auth-user-pass /etc/openvpn/PIA-Authentication.txt`.

## Setup Firewall for VPN

1.  Enable outbound blocking to prevent leaks if the VPN disconnects
    ```
    sudo ufw default deny outgoing
    ```

2.  Find the IPs for your configured DNS server\(s\):
    ```
    cat /etc/network/interfaces
    ```

3.  Allow outbound connections to the DNS server\(s\). Run the command for each IP:
    ```
    sudo ufw allow out on interface_name to dns_server_ip port 53 comment "DNS"
    ```

1.  Allow the VPN connection through the firewall
    ```
    sudo ufw allow out on <lan_interface> to any proto udp port 1198 comment "PIA VPN"
    ```

2.  Allow all outbound connections over the tunnel
    ```
    sudo ufw allow out on tun0 to any comment "Outbound Tunneled Traffic"
    ```

## Test Your Configuration File

1.  Run OpenVPN with your configuration file
    ```
    sudo openvpn /etc/openvpn/PIA-<Location>.conf
    ```

## Enable OpenVPN On Boot

1.  Check your current IP address. Make a note. This is your ISP issued IP address.
    ```
    curl https://icanhazip.com
    ```

2.  Enable the systemd service for your config file to automatically start on boot
    ```
    sudo systemctl enable openvpn@PIA-<Location>.service
    ```

3.  Start the openvpn service
    ```
    sudo systemctl start openvpn@PIA-<Location>.service
    ```

4.  Check the status to make sure you have connected correctly
    ```
    sudo systemctl status -l openvpn@PIA-<Location>.service
    ```

5.  Check your IP. It should be different than your ISP issued IP address.
    ```
    curl https://icanhazip.com
    ```

6.  Remove the temporary HTTP & HTTPS firewall rule (If you fail to do this then connections can leak)
    ```
    sudo ufw delete allow out to any port 80
    sudo ufw delete allow out to any port 443
    ```

## Setup automatic restart of VPN service

1.  Create a service config file to append to the existing configuration
    ```
    sudo systemctl edit openvpn@PIA-<Location>.service
    ```

2.  Paste the following into the text editor then save
    ```
    [Service]
    Restart=always
    RestartSec=30
    ```

3.  Check your resulting configuration
    ```
    sudo systemctl cat openvpn@PIA-<Location>.service
    ```

4. You should see your changes in the section
    ```
    # /etc/systemd/system/openvpn@PIA-<Location>.service.d/override.conf
    ```

5.  Test the automatic restart by disconnecting from the internet. Check the service status. It might take a minute or two for it to realize the connection is dead. You should see something like “TLS Error: TLS key negotiation failed to occur within 60 seconds (check your network connectivity)”
    ```
    sudo systemctl status -l openvpn@PIA-<Location>.service
    ```

6.  Now reconnect to the internet and wait a few minutes for it to attempt to reconnect. Rerun the above command. You should now see:

## Configure Port Fowarding (Work in Progress)

1.  Create the port forwarding script
    ```
    sudo nano 
    ```

3.  Paste in the following script
    ```
    #!/usr/bin/env bash
    #
    # Enable port forwarding when using Private Internet Access
    #
    # Usage:
    #  ./port_forwarding.sh
    
    error( )
    {
      echo "$@" 1>&2
      exit 1
    }
    
    error_and_usage( )
    {
      echo "$@" 1>&2
      usage_and_exit 1
    }
    
    usage( )
    {
      echo "Usage: `dirname $0`/$PROGRAM"
    }
    
    usage_and_exit( )
    {
      usage
      exit $1
    }
    
    version( )
    {
      echo "$PROGRAM version $VERSION"
    }
    
    
    port_forward_assignment( )
    {
      echo 'Loading port forward assignment information...'
      if [ "$(uname)" == "Linux" ]; then
        client_id=`head -n 100 /dev/urandom | sha256sum | tr -d " -"`
      fi
      if [ "$(uname)" == "Darwin" ]; then
        client_id=`head -n 100 /dev/urandom | shasum -a 256 | tr -d " -"`
      fi
    
      json=`curl "http://209.222.18.222:2000/?client_id=$client_id" 2>/dev/null`
      if [ "$json" == "" ]; then
        json='Port forwarding is already activated on this connection, has expired, or you are not connected to a PIA region that supports port forwarding'
      fi
    
      echo $json
    }
    
    EXITCODE=0
    PROGRAM=`basename $0`
    VERSION=2.1
    
    while test $# -gt 0
    do
      case $1 in
      --usage | --help | -h )
        usage_and_exit 0
        ;;
      --version | -v )
        version
        exit 0
        ;;
      *)
        error_and_usage "Unrecognized option: $1"
        ;;
      esac
      shift
    done
    
    port_forward_assignment
    
    exit 0
    ```

### [>> Next Step: Setup File Storage](server/setup.md#setup-file-storage)