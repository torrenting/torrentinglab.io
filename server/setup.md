# Setup Ubuntu Server


## Install Ubuntu Server

Install Ubuntu Server on a VM, docker, or cloud service

## Setup Public Key Authentication

Using a key to login to your server greatly improves the security of your server. It is essential for any server exposed to the open internet.

[>> Setup Public Key Authentication](/server/public-key-authentication.html)

## Update System

Before continuing make sure your system is up to date!

1. Download the list of updates availible:
    ```
    sudo apt update
    ```
2. Update packages and and apply security fixes:
    ```
    sudo apt upgrade
    ```

## Configure Firewall

A firewall is required to protect against port binding mistakes and other security related misconfiguration that can happen on any server.
A firewall is your first line of defense in a system with multiple security layers. It prevents unexpected or unauthorized connections from happening in the first place.

1.  Enable inbound blocking:
    ```
    sudo ufw default deny incoming
    ```

3.  Find the name of your network interface. Look for your network address in the list of interfaces. The interface name is listed after the interface number (eg eth0):
    ```
    ip address show
    ```

4.  Allow incoming SSH connections:
    ```
    sudo ufw allow in on interface_name from any to any port 22 comment "SSH"
    ```

5.  Enable UFW. Make sure you allowed SSH or you will lose your connection. You may have to hit enter twice after entering y for the prompt:
    ```
    sudo ufw enable
    ```

8.  Check your set rules:
    ```
    sudo ufw status verbose
    ```

*   When you need to allow something through the firewall
    *   Enable logging so you can troubleshoot failed connections:
        ```
        sudo ufw logging on
        ```

    *   To see what ports a service is trying to use:
        ```
        sudo less /var/log/ufw.log
        sudo netstat -natp
        sudo rpcinfo -p
        ```

## Create a SFTP User
Create a new user that will be used to access and manage your files. This user will not have administrative access. You can have your SFTP client remember the login without risk to your server's security.

1. Create the SFTP user:
    ```
    sudo adduser sftp_user
    ```

2. Generate a new SSH key for your user

[>> Generate SSH Login Key on Client Computer](/server/public-key-authentication.html#generate-ssh-login-key-on-client-computer)

3. Add the SSH key to the SFTP account
    1. Switch to the SFTP user:
        ```
        sudo su - sftp_user
        ```
    2.  Create the .ssh directory:
        ```
        mkdir .ssh
        chmod u=rwX,g=,o= .ssh
        ```
    3.  Copy the public key and paste into the authorized_keys file on the server:
        ```
        echo "your_public_key" >> .ssh/authorized_keys
        ```
    4.  Change the permissions so only your user can read the file:
        ```
        chmod u=rw,g=,o= .ssh/authorized_keys
        ```
    5.  Exit the SFTP user session:
        ```
        exit
        ```

## Setup File Storage

Setup a place to store your downloaded files.

[>> Setup Shared Storage](/server/file-system.html)  
[>> Test & Format Drives](/server/test-format-drives.html)

## Setup OpenVPN Client

If your server is located at home it's highly recommended to use a VPN service to protect yourself.
Even on private trackers your IP can be leaked when peer lists are leaked to other less reputable trackers by malicious users.
Some VPN providers support port forwarding so you can remain connectable.

[>> Setup OpenVPN Client for Private Internet Access \(PIA\)](/server/openvpn-client-PIA.html)

[>> Setup OpenVPN Client for AirVPN ](/server/openvpn-client-AirVPN.html)

## Install Daemons

Now install the services you want on your server