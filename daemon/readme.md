# Setup a Torrent Daemon

Choose a torrent daemon that fits your needs. No daemon is perfect and each has it’s pros and cons. Choose one that fits your needs the best instead of what everyone says you should be using.

## [Deluge](/daemon/deluge.html)

Pros  
*  Aggressive Peering meaning you’re more likely to win the race to seed something. Difference is more noticeable on fast connections (Gigabit)
*  Supports file and folder renaming including completely changing directory structure
*  Supports both a webUI and a remotely connected native client
*  Plugins to extend functionality on both client and server

Cons  
*  Higher memory use with default settings than rtorrent
*  Large numbers of torrents with lots of information displayed in columns on the GUI can cause high cpu usage and slow view changes when using the GUI

## [rTorrent](/daemon/rtorrent.html)

Pros  
*  Daemon has efficient memory usage
*  Daemon handles very large numbers of torrents (10k+)

Cons  
*  WebUI can be memory and CPU intensive with large numbers of torrents and may require tweaking php.ini
*  Doesn’t support file renaming (needs verification)
*  No native GUI client

## qBitrorrent

## Transmission