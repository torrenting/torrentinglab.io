# Deluge on Ubuntu Server

## Install Deluge

1.  Add the Deluge PPA
    ```
    sudo add-apt-repository ppa:deluge-team/ppa
    ```
2.  Make sure you have the most recent packages
    ```
    sudo apt update
    ```
3.  Install deluged and deluge-console
    ```
    sudo apt install deluged deluge-console
    ```

## Prepare Users

1.  Create the deluged user
    ```
    sudo adduser --home /var/lib/deluged deluged
    ```
2.  Fix the permissions on the deluge config directory
    ```
    sudo chown -R deluged:deluged /var/lib/deluged
    ```
3.  Add your other users to the deluged group
    ```
    sudo usermod -aG deluged username
    ```

## Setup Deluged

1.  Remove the legacy init.d script
    ```
    sudo rm /etc/init.d/deluged
    sudo update-rc.d deluged remove
    ```
2.  Create the deluged service file
    1.  Create a new system service file. This will automatically override the init.d script that is installed with the deluged package.
        ```
        sudo nano /etc/systemd/system/deluged.service
        ```
    2.  Paste the following into the service file
        ```
        [Unit]
        Description=Deluge Bittorrent Client Daemon
        
        # Start after network, vpn, and specified mounts are available.
        After=network-online.target openvpn@vpn_name.service <mount_point>.mount
        
        # Stops deluged if mount points disconnect
        BindsTo=mount_point.mount
        
        [Service]
        Type=simple
        User=deluged
        Group=<shares_group_name>
        UMask=002
        
        ExecStart=/usr/bin/deluged -d -c /var/lib/deluged/config
        
        Restart=on-failure
        
        # Configures the time to wait before service is stopped forcefully.
        TimeoutStopSec=300
        
        [Install]
        WantedBy=multi-user.target <mount_point>.mount
        ```
        1.  Change <shares_group_name> to match your group name used on your file store. If you’re using SSHFS then it will be whatever group you assigned the mount to.
        2.  Note the umask of 002 which will cause the service to create any files with rw access for both the deluged user and the group <shares_group_name>; and read only access for others
        3.  If you are using SSHFS add the names of the mount services where it says <mount_point>.mount; otherwise omit those entries.
2.  Tell systemd to reload service configurations
    ```
    sudo systemctl daemon-reload
    ```
3.  Start the service to trigger the generation of the config files
    ```
    sudo systemctl start deluged
    ```
4.  Check the service status to ensure everything is running correctly
    ```
    sudo systemctl status -l deluged
    ```
5.  Stop the service so we can configure remote connections
    ```
    sudo systemctl stop deluged
    ```
6.  Configure the deluged daemon for remote administration
    1.  Edit the auth file
        ```
        sudo nano /var/lib/deluged/config/auth
        ```
    2.  Add a line for your desired username and password. 10 denotes the admin auth level.
        ```
        username:password:10
        ```
    3.  Enable remote connection by editing core.conf
        ```
        sudo nano /var/lib/deluged/config/core.conf
        ```
    4.  Find the line
        ```
        "allow_remote": false,
        ```
    5.  Change to
        ```
        "allow_remote": true,
        ```
    6.  Change the daemon_port number to something random and make a note of the port number in the same place you recorded the the user:pass for remote connections.
        ```
        "daemon_port": <daemon_port>,
        ```
    7.  Save the configuration file
7.  Allow the daemon admin interface thru the firewall. Connections are limited to the lan interface to prevent admin interface connections coming from the vpn tunnel. <lan_interface> is your non-vpn interface such as eth0. You can find the name with $ ip link show
    *  If your server is on a lan and you administer from the lan (192.168.1.0/24)
        ```
        sudo ufw allow in on <lan_interface> from <lan_subnet>/24 to any port <daemon_port> comment "deluged administration"
        ```
    *  If your server is on a lan that you administer via vpn (192.168.2.0/24)
        ```
        sudo ufw allow in on <lan_interface> from <vpn_subnet>/24 to any port <daemon_port> "deluged administration"
        ```
    *  If your server is on the internet I would strongly recommend against exposing the deluged administration port to the internet. Safer to use a SSH tunnel. This will allow you to connect to your daemon using 127.0.0.1:<client_daemon_port> instead of the server IP and port and the connection will be tunneled to your server over your encrypted and secured ssh connection. -N specifies this is a tunnel only connection and cannot be used to run commands on the server. On your client computer run the following. 
        ```
        ssh -N -p <ssh_port> -L <client_daemon_port>:127.0.0.1:<daemon_port> -i ~/.ssh/<ssh_private_key> -o IdentitiesOnly=yes <username>@<server_ip>
        ```
8.  Start the deluged service
    ```
    sudo systemctl start deluged
    ```
9.  Check the service status to ensure everything is running correctly
    ```
    sudo systemctl status -l deluged
    ```
10. Enable the service so it starts on boot
    ```
    sudo systemctl enable deluged
    ```

## Connect to deluged using the GTK client

Install Deluge on your client computer, put it into thin client mode, and connect to the deluged daemon.

1.  Install Deluge on your client computer and then run it.
2.  Open the Preferences

3.  Go to the Interface section. Un-check the Enable button under Classic Mode. Restart Deluge.
   
4.  Open the Connection Manager
   
5.  Click Add
   
6.  Fill in your deluged server information and click Add
   
7.  Expand the Options. Make sure localhost autostart is disabled. Enable automatically connecting to your server when you start Deluge. Disable showing the connection dialog on start.
    

## Configure deluged Preferences

1.  Once you are connected to the server open up Preferences.
   
2.  Downloads Tab:
       
    1.  Set the download folder to your storage mount point
    2.  Set Allocation to Use Full Allocation to minimize fragmentation
3.  Network Tab:
       
    1.  Incoming Ports: Uncheck “Use Random Ports” and specify a single port to use for incoming connections. This is the port that will be port forwarded through the VPN connection.
    2.  Outgoing Ports: Uncheck “Use Random Ports” and specify a port range that doesn’t overlap with your incoming port or your daemon port. Your range should be big enough to fit the maximum connections specified in the bandwidth tab since deluged seems to use a different port for every outgoing connection.
4.  Bandwidth Tab:
The values you set for these options will depend on your connection speed. For a more in depth guide see: http://dev.deluge-torrent.org/wiki/UserGuide/BandwidthTweaking
       
    1.  Test your sustained download speed and set conservative max
        1.  Ensure you have nothing else utilizing your connection then run a large file speed test to get an idea of your sustained download speed without any “burst” speeds that some ISPs provide.
               $ wget -O /dev/null http://speedtest.wdc01.softlayer.com/downloads/test600.zip
           
        2.  Towards the end of the download make a note of the speed. Divide that number by 0.001024 to get the speed in KiB/s (6.72 ÷ 0.001024 = 6562.5). Multiply by 0.90 to get 90% of that number (6562.5×0.90 = 5906.25). Round to the nearest whole number. This is the number you want to enter for Maximum Download Speed.
            1.  If you have a slower connection you might want to give yourself more of a buffer. If a torrent download saturates your connection then speed will suffer so setting the max conservatively helps prevent any problems.
    2.  Test your upload speed and set conservative max. Take your ISP advertised upload speed (10Mbps) and divide by 0.008192 to get the speed in KiB/s (10 ÷ 0.008192 = 1220.703125). Multiply by 0.80 to get 80% of that number (1220.703125 × 0.80 = 976.5625). Round to the nearest whole number. This is the number you want to enter for Maximum Upload Speed.
        1.  Testing your upload speed is problematic. Most speed test sites do not test for long enough to get an accurate measurement of non-burst upload speed. It’s best to use a speed test site to ensure you’re getting at least your ISP advertised upload speed and then set the setting based off what you have been sold instead of what the speed test told you your max is since it will be highly optimistic.
        2.  If you have a slower connection you might want to give yourself more of a buffer.
    3.  The maximum connections controls how many open connections you can have to peers. The higher the number of active connections, the higher your protocol overhead is. Protocol bandwidth usage is displayed on the bottom bar of the program. 
        1.  If you’re seeding thousands of torrents you probably want to set this to a higher number than the default. If your protocol overhead starts choking your connection then you want to lower the maximum.
    4.  Maximum upload slots control how many open upload connections you can have to other peers. It does not affect how many torrents you can have idle seeding. The optimal setting will depend on your connection speed. The faster it is the more connections you can handle without spreading your bandwidth too thin. Peers will choose not to connect to you in favor of a faster peer if you have too many upload slots open and are spreading your bandwidth too thin. Supposedly if you are not utilizing your full upload bandwidth and you have peers asking for a connection then deluged will temporarily create more upload slots to fill that demand. A rule of thumb you could use is to have one upload slot for every 100 KiB/s of upload you can provide.
5.  Queue Tab:
       
    1.  Set the Total active & active seeding to -1 for unlimited.
        1.  You could leave these at the defaults if you want to expire torrents. But you’re running a server so the assumption is you’re seeding as much as possible for eternity.
        2.  On old hardware (Core2 Duo) I have been able to seed 2000 torrents with very minimal resource usage. Having the GTK UI connected actually consumes the lion’s share of CPU cycles. You will encounter lag and torrents not updating their status in the GUI if you are overloaded. Avoid using the ALL filter unless you really need to.
    2.  Total active downloading should be small to be downloading efficiently.
    3.  Check the box “Do not count slow torrents” to so deluged ignores the download limits on really slow torrents to prevent blocking faster ones.

## Allow Deluged Through the Firewall

1.  Allow the incoming port through the firewall. Connections are limited to the vpn interface to prevent traffic leaks over your ISP connection. <vpn_interface> is your vpn interface such as tun0. You can find the name with $ ip link show
    ```
    sudo ufw allow in on <vpn_interface> to any port <daemon_incoming_port> comment "deluged incoming"
    ```
2.  Allow all outbound connections on the vpn interface through the firewall (This is nesssasary because trackers will use various ports for the announce url). Connections are limited to the vpn interface to prevent traffic leaks over your ISP connection. <vpn_interface> is your vpn interface such as tun0. You can find the name with $ ip link show
    ```
    sudo ufw allow out on <vpn_interface> to any comment "deluged outgoing"
    ```

## Test Your Configuration

1.  It’s not secure till you’ve tested to ensure it really is. Download a torrent file for a linux iso and load it in Deluge. You should see the tracker give a response and start downloading. If you don’t, check your firewall rules.
2.  Disable the VPN connection
    ```
    sudo service openvpn@PIA-<Location> stop
    ```
3.  Right click torrent and choose “Update Tracker”. The tracker status should say “Announce Sent” then after a few seconds it will change to something similar to ”Error: Connection timed out”.
    1.  If it changes to “Announce OK” then you are leaking traffic!
    2.  Make sure you have set ufw to outbound default deny.
    3.  Check your firewall rules for any generic outgoing entries that would allow traffic out over the lan. Any traffic allowed out over the lan should be restricted to specific IPs and ports.
    4.  You deleted the temporary rules used earlier right?
4.  Now start the vpn again and reannounce to the tracker.
    ```
    sudo service openvpn@PIA-<Location> start
    ```

## Setup backups
Periodically backup your config and state so you can recover should your state get corrupted or deleted

1.  Create backups directory
    ```
    sudo mkdir -p /backups/deluge
    sudo chown root:backup /backups/deluge
    sudo chmod o= /backups/deluge
    sudo setfacl -dm o::- /backups/deluge
    ```

2.  Create backup script
    ```
    nano deluge-backup.sh
    ```

3.  Paste in the following and save
    ```
    #!/bin/bash
    
    config_dir="/var/lib/deluged"
    backup_dir="/backups/deluge"
    todays_date="$(date +%FT%H%MZ)"
    log_file="${backup_dir}/backup-progress.log"
    expiration_days="30"
    full_backup_present=false
    
    # Use this to echo to standard error
    error () {
        printf "%s: %s\n" "$(basename "${BASH_SOURCE}")" "${1}" >&2
        exit 1
    }
    
    trap 'error "An unexpected error occurred."' ERR
    
    sanity_check () {
        # Ensure user has write access
        if [ ! -w "${backup_dir}" ]; then
            error "Script does not have write access to backup directory"
        fi
    }
    
    take_backup () {
        cd ${config_dir}
        tar -czf "${backup_dir}/${todays_date}.tar.gz" "config/" 2> "${log_file}"
    }
    
    cleanup_backups () {
        # Check if there's a recent backup
        if find "${backup_dir}/" -ctime -1 -type f -name "*.tar.gz"; then
            full_backup_present=true
        fi
        # Remove old backups if there is a recent backup present
        if [ "${full_backup_present}" = "true" ]; then
            find "${backup_dir}/" -ctime +${expiration_days} -delete;
            echo "Backups older than ${expiration_days} days deleted"
        else
            error "Removal of old backups prevented due to a backup failure"
        fi
    }
    
    sanity_check && cleanup_backups && take_backup
    ```

4.  Make the backup script executable
    ```
    chmod +x deluge-backup.sh
    ```

5.  Move to the user binaries folder
    ```
    sudo mv deluge-backup.sh /usr/local/bin/
    ```

6.  Create a service to run the backup script
    ```
    sudo nano /etc/systemd/system/deluge-backup.service
    ```

7.  Paste in the following and save
    ```
    [Unit]
    Description=Backup deluge configuration
    
    [Service]
    Type=oneshot
    User=root
    Group=backup
    ExecStart=/usr/local/bin/deluge-backup.sh
    ```

8.  Create a timer to trigger creation of a backup
    ```
    sudo nano /etc/systemd/system/deluge-backup.timer
    ```

9.  Paste in the following and save
    ```
    [Unit]
    Description=Backup deluge configuration periodically
    
    [Timer]
    OnStartupSec=5min
    OnUnitInactiveSec=12hr
    
    [Install]
    WantedBy=timers.target
    ```

10.  Enable the timer
```
sudo systemctl enable deluge-backup.timer
sudo systemctl start deluge-backup.timer
```

## Install plugins

1.  Find out what version of python you are using
    1.  On the server (probably 2.7.x)
        ```
        python --version
        ```
    2.  On your client
        ```
        python --version
        ```
    3.  If they’re both the same major version (2.7.x or 3.4.x) then you can use the GUI to add the plugin. If your server has an older version than your client then you will need to install the plugin on the the server manually.
    4.  If your server has a different python version:
        1.  Download the plugin release on the server
            ```
            wget <download-url>
            ```
        2.  Copy the .egg file to the deluge plugin directory
            ```
            sudo cp <plugin_filename> /var/lib/deluged/config/plugins/
            ```
        3.  Set the permissions to ensure the daemon can load the plugin
            ```
            sudo chmod u=rw,g=rw,o=r /var/lib/deluged/config/plugins/<plugin_filename>
            sudo chown deluged:deluged /var/lib/deluged/config/plugins/<plugin_filename>
            ```
        4.  Make sure permissions are correct. Should look something like this
-rw-rw-r-- 1 debian-deluged debian-deluged <plugin_filename>
            ```
            sudo ls -l /var/lib/deluged/config/plugins
            ```
    5.  If the versions are the same skip to the next step
2.  Install the plugins via the GUI
    1.  Open preferences in Deluge after connecting to the server daemon.
    2.  Navigate to the Plugins tab and click “Install Plugin”
       
    3.  Find the .egg file and open it
       
    4.  Enable the plugin
       

## Install ltConfig plugin

The ltConfig plugin allows you to change the default libtorrent settings from the GUI. It comes with a few presets designed for different types of usage scenarios. This is required if you will have more than 1600 torrents in deluge due to an unfixed bug with max tracker connections.

1.  https://github.com/ratanakvlun/deluge-ltconfig/releases
2.  Download the latest release
3.  Install the plugin using the steps for installing plugins
4.  Go to the ltConfig tab
5.  Choose the “High Performance Seed” preset and click “Load Preset”
6.  Check the box next to “active_tracker_limit” and set it to “-1” for unlimited
```
“cache_buffer_chunk_size” “128”
“cache_expiry” “30”
“cache_size” “65536”
“connection_speed” “200”
“file_pool_size” “500”
“inactivity_timeout” “20”
“listen_queue_size” “3000”
“low_prio_disk” “unchecked”
“max_allowed_in_request_queue” “2000”
“max_http_recv_buffer_size” “6291456”
“max_out_request_queue” “1500”
“max_queued_disk_bytes” “7340032”
“send_buffer_low_watermark” “1048576”
“send_buffer_watermark” “3145728”
“send_buffer_watermark_factor” “150”
“send_socket_buffer_size” “1048576”
“suggest_mode” “1”
“unchoke_slots_limit” “2000”
```

## Install browsebutton plugin

Install the very useful browse button plugin. This plugin allows you to browse for a directory to save a torrent in while connected remotely. Without it you will have to manually type the destination directory.

1.  Visit https://github.com/dredkin/deluge-rbb/releases
2.  Download the latest release for your python version. Note that the Deluge client can crash when using 0.1.10 so you might want to use 0.1.9 instead.
3.  Install the plugin using the steps for installing plugins
