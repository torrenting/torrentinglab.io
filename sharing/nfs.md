# Install & Configure NFS

1. Install NFS
    ```
    sudo apt install nfs-kernel-server
    ```
2. Create NFS export root directories
    ```
    sudo mkdir -p /srv/nfs4/<nfs_directory>
    ```
3. Add bind points to fstab
    1. Edit fstab file
        ```
        sudo nano /etc/fstab
        ```
    2. Add lines for each export directory. If your directory name has spaces then replace the space with \040
        ```
        /srv/share_name /srv/nfs4/share_name none ro,bind 0 0
        ```
    3. (Optional) Temporarily create bind mounts if you don't want to restart to have them auto mount
        ```
        sudo mount -o bind /srv/share_name /srv/nfs4/share_name
        ```
4. Add entries to exports
    1. Edit exports file
        ```
        sudo nano /etc/exports
        ```
    2. Add lines for each directory. Replace <local_network> with the network range for your network ex: 10.1.10.0/24
        ```
        /srv/nfs4/share_name <local_network>(ro,all_squash,insecure)
        ```
    3. Refresh the exports
        ```
        sudo exportfs -rav
        ```
5. Configure the firewall for nfs
    1. Set static ports for the mountd nfs service (normally uses a random port)
        1. Edit the configuration file
           ```
           sudo nano /etc/default/nfs-kernel-server
           ```
        2. Replace
           ```
           RPCMOUNTDOPTS="--manage-gids"
           ```
        3. With
           ```
           RPCMOUNTDOPTS="--manage-gids --port 32767"
           ```
    2. Add firewall rules to allow traffic in & out. Replace <lan_subnet> with the network range for your server ex: 10.1.10.0
        1. Portmapper
           ```
           sudo ufw allow in from <lan_subnet>/24 to any port 111 comment "Portmapper"
           ```
        2. NFS
           ```
           sudo ufw allow in from <lan_subnet>/24 to any port 2049 comment "NFS"
           ```
        3. mountd
           ```
           sudo ufw allow in from <lan_subnet>/24 to any port 32767 comment "NFS mountd"
           ```
        4. Unknown, Needed for browsing for nfs servers (idmapd statd both disabled)
           ```
           sudo ufw allow in from <lan_subnet>/24 to any port 36434 comment "NFS"
           ```
        5. Unknown, Needed for browsing for nfs servers (idmapd statd both disabled)
           ```
           sudo ufw allow in from <lan_subnet>/24 to any port 56946 comment "NFS"
           ```
    3. Restart UFW Firewall to apply changes
       ```
       sudo service ufw restart
       ```
6. Restart NFS server to save changes
   ```
   sudo service nfs-kernel-server restart
   ```