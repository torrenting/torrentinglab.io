# Backup Your Data

## Check the Backup Drive Health

1. Run a SMART test to ensure electronics are working properly
   ```
   sudo smartctl -t short /dev/<device>
   ```
2. Check the results. Check the following for any non zero results: Reallocated_Sector_Ct (5), Reported_Uncorrect (187), Command_Timeout (188), Current_Pending_Sector (197), and Offline_Uncorrectable (198)
   ```
   sudo smartctl -a /dev/<device>
   ```

## Sync Changes

1. Mount the drive
2. Start a screen session so the syncing process continues if you get disconnected or logout.
   ```
   screen
   ```
3. Check the changes to ensure you have selected the right drives
   ```
   rsync --recursive --perms --times --delete-before --progress --dry-run --itemize-changes <source_with_trailing_slash> <destination> | less
   ```
4. Sync changes including deletions
   ```
   rsync --recursive --perms --times --delete-before --progress <source_with_trailing_slash> <destination>
   ```
5. Detatch the currently running screen so it runs in the background. Press Ctrl+A then press D
    * You can check on the progress at any time by entering the following command
        ```
        screen -r
        ```
    * If you had multiple screens detached then it will list all active ones. Copy the name of the one you want (####.pts-0.hostname) then enter
        ```
        screen -r ####.pts-0.hostname
        ```
7. When you’re finished enter “exit” to close the currently open screen or press Ctrl+A then K.
8. Umount the drive