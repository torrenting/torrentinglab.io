# Maintaining Your Hard Drives

## Verifying Data on a BTRFS drive

*  Start the drive scrub (verifies checksums and data)
    ```
    sudo btrfs scrub start /srv/mount_point
    ```

*  Check on progress/results
    ```
    sudo btrfs scrub status /srv/mount_point
    ```

## If you need to do something that requires unmounting a drive first

1.  Disable any NFS exports. Comment out the drive you want to unmount in the export list (# at beginning of line)
    ```
    sudo nano /etc/exports
    ```
2.  Reload exports and the drive will no longer be busy
    ```
    sudo exportfs -rav
    ```
3.  Unmount your drive
    ```
    sudo umount /dev/drive_name
    ```

## Check and fix a broken filesystem

