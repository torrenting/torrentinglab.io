# Torrent Server Setup Guide

This guide is intended to help users setup a torrent server for seedboxes and home servers.  
It is written for Ubuntu Server 18.04 LTS. It might work on other versions and distros with adjustments.

Includes instructions for things commonly needed for home servers including:

*  Testing and formatting drives (to-do)
*  Setting up media shares and plex (to-do)
*  Preventing leaks while connected to a VPN
*  Forwarding ports while connected to a VPN (to-do)
*  Torrent daemon configuration for seeding large amounts of torrents

## Get Started

Prepare and secure your server:  
[>> Server Setup](server/setup.md)

Setup the torrent daemon of your choice:  
[>> Daemon Setup](daemon/readme.md)